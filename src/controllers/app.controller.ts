// app.controller.ts
import { Guid } from 'guid-typescript';
import { Request, Response } from 'express';
import { Controller, Inject, Get, Post, Req, Res, Body, HttpStatus, UsePipes, UseInterceptors } from '@nestjs/common';
import { ZBClient } from 'zeebe-node';
import { CreateWorkflowInstanceResponse } from 'zeebe-node/interfaces';
import { ZEEBE_CONNECTION_PROVIDER, ZeebeWorker, ZeebeServer } from '@payk/nestjs-zeebe';
import { Consumer } from 'rabbitmq-provider/consumer';
import { RabbitmqConsumerService } from '../services/rabbitmq-consumer.service';

@Controller()
export class AppController {
  responses = new Map<string, Response>();

  constructor(
    @Inject(ZEEBE_CONNECTION_PROVIDER) private readonly zbClient: ZBClient,
    private readonly zeebeServer: ZeebeServer,
    private readonly rabbitmqConsumerService: RabbitmqConsumerService, // gateway consumer
  ) {
    // Gateway consumer
    this.rabbitmqConsumerService.createAndStartProcessing((_, messages) => {
      const payloads = Consumer.getPayloads(messages);
      console.log(`@@@ send back events: ${JSON.stringify(messages)}`);
      payloads.forEach(item => this.sendResultToClient(item.sessionId, item.result));
    })
      .then(() => console.log('Gateway consumer created and started processing'));
  }

  sendResultToClient(sessionId: string, result: any) {
    const response = this.responses.get(sessionId);
    if (response) {
      response.status(HttpStatus.OK).send(`Workflow result -> ${JSON.stringify(result)}`);
      this.responses.delete(sessionId);
    }
  }

  // Use the client to create a new workflow instance
  @Get()
  async start(@Req() req: Request, @Res() res: Response): Promise<CreateWorkflowInstanceResponse> {
    const sessionId = `${Guid.create()}`;
    const wfi = await this.zbClient.createWorkflowInstance('order-process', {
      sessionId,
      tracer: 'init',
    });

    console.log(`getHello() ${JSON.stringify(wfi)}`);

    this.responses.set(sessionId, res);
    return wfi;
  }
}
